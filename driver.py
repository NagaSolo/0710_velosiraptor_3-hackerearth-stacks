''' CLI program as driver'''
from stacks import Pelayan
if __name__ == '__main__':
    program_berjalan = True
    drink_session = Pelayan()
    while program_berjalan:
        pilihan = input('\n1 to receive order\n2 to delivered the order\n3 to display current order\nq to quit\n')
        if (pilihan == '1'):
            drink_name = input('\nName your drink:\n')
            drink_session.receive_order(drink_name)
        elif (pilihan == '2'):
            drink_session.order_delivered()
            print('\nOrder has been delivered\n')
        elif (pilihan == '3'):
            print(drink_session)
        else:
            program_berjalan = False
