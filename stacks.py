''' Implement stacks using list, FIFO'''

class Pelayan(list):
    def __init__(self):
        super(Pelayan, self).__init__()
        self.list_of_drinks = []

    def receive_order(self, drink_name):
        self.list_of_drinks.append(drink_name)

    def order_delivered(self):
        self.list_of_drinks.pop()

    def display_order(self):
        print(self.list_of_drinks)